'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
    body: {
        type: String,
        required: true,
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    sender: {
        type: String,
        required: true,
    },
    receiver: {
        type: String,
        required: true,
    },
    status: {
        type: [{
            type: String,
            enum: ['active', 'archived', 'deleted']
        }],
        default: ['active']
    }
});

module.exports = mongoose.model('Message', MessageSchema);
