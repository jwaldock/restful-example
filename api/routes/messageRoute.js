'use strict';
module.exports = function(app) {
    let messageController = require('../controllers/messageController');


    // todoList Routes
    app.route('/messages')
        .get(messageController.listAllMessages)
        .post(messageController.createMessage);

    app.route('/messages/:messageId')
        .get(messageController.readMessage)
        .put(messageController.updateMessage)
        .delete(messageController.deleteMessage);
};
